$(document).ready(function() {

    $('.js-switch').on('click', function() {

        if ($('.js-switch').hasClass('active')) {
            $('.js-switch').removeClass('active');
            $('.js-switch-circle').animate({left: "25px", left: "7px"}, 200);
        } else {
            $('.js-switch').addClass('active');
            $('.js-switch-circle').animate({left: "7px", left: "25px"}, 200);
        }
    });

    var heightPage = $(document).outerHeight(true);

    var heightTop = $('.js-top').outerHeight(true);

    var heightBottom = $('.js-bottom').outerHeight(true);

    var heightMiddle = heightPage - heightTop - heightBottom;

    $('.js-middle').css('height',heightMiddle);

});
