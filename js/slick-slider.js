$(document).ready(function() {
    var num;

    // Получаем индекс активного элемента
    $('.block-choice-of-workplace__li').each(function (index, value) {
        if ($(this).hasClass('active')) {
            num = $(this).index();
        }
    });

    // Карусель с рабочими местами на странице Календарь
    $('.js-calendar-slider').slick({
        infinite: false,
        slidesToShow: 10,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<a href="#" class="block-choice-of-workplace__arrow-link block-choice-of-workplace__arrow-link_left slick-prev"><svg class="block-choice-of-workplace__arrow-icon"><use xlink:href="img/sprite.svg#arrow-slider" /></svg><svg class="block-choice-of-workplace__arrow-icon block-choice-of-workplace__arrow-icon_hover"><use xlink:href="img/sprite.svg#arrow-slider-hover" /></svg></a>',
        nextArrow: '<a href="#" class="block-choice-of-workplace__arrow-link slick-next"><svg class="block-choice-of-workplace__arrow-icon"><use xlink:href="img/sprite.svg#arrow-slider" /></svg><svg class="block-choice-of-workplace__arrow-icon block-choice-of-workplace__arrow-icon_hover"><use xlink:href="img/sprite.svg#arrow-slider-hover" /></svg></a>',
        focusOnSelect: true,
        // initialSlide: num,
    });

    $('.js-meetings-slider').slick({
        infinite: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<a href="#" class="block-choice-of-workplace__arrow-link block-choice-of-workplace__arrow-link_left slick-prev"><svg class="block-choice-of-workplace__arrow-icon"><use xlink:href="img/sprite.svg#arrow-slider" /></svg><svg class="block-choice-of-workplace__arrow-icon block-choice-of-workplace__arrow-icon_hover"><use xlink:href="img/sprite.svg#arrow-slider-hover" /></svg></a>',
        nextArrow: '<a href="#" class="block-choice-of-workplace__arrow-link slick-next"><svg class="block-choice-of-workplace__arrow-icon"><use xlink:href="img/sprite.svg#arrow-slider" /></svg><svg class="block-choice-of-workplace__arrow-icon block-choice-of-workplace__arrow-icon_hover"><use xlink:href="img/sprite.svg#arrow-slider-hover" /></svg></a>',
        focusOnSelect: true,
    });

    $('.js-block-photos-slider').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<a href="#" class="block-photos-slider__arrow-link block-photos-slider__arrow-link_left slick-prev"><svg class="block-photos-slider__arrow-icon"><use xlink:href="img/sprite.svg#arrow-slider-white" /></svg><svg class="block-photos-slider__arrow-icon block-photos-slider__arrow-icon_hover"><use xlink:href="img/sprite.svg#arrow-slider-hover" /></svg></a>',
        nextArrow: '<a href="#" class="block-photos-slider__arrow-link slick-next"><svg class="block-photos-slider__arrow-icon"><use xlink:href="img/sprite.svg#arrow-slider-white" /></svg><svg class="block-photos-slider__arrow-icon block-photos-slider__arrow-icon_hover"><use xlink:href="img/sprite.svg#arrow-slider-hover" /></svg></a>',
        focusOnSelect: true,
    });

});
